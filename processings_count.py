import re

saga = 0
native = 0
grass = 0
gdal = 0
qgis = 0
_3d = 0
others = []

for alg in QgsApplication.processingRegistry().algorithms():
    m = re.search(r'(^;*)\w+', alg.id()).group(0)
    if m == "saga":
        saga += 1
    elif m == "grass7":
        grass += 1
    elif m == "gdal":
        gdal+=1
    elif m == "qgis":
        qgis +=1
    elif m == "native":
        native +=1
    elif m == "3d":
        _3d +=1
    else:
        others.append(m)

if others:
    for other in others:
        print(other)

print(f"there is {qgis} qgis processing algorithms.")
print(f"there is {native} native processing algorithms.")
print(f"there is {saga} saga processing algorithms.")
print(f"there is {grass} grass processing algorithms.")
print(f"there is {gdal} gdal processing algorithms.")
print(f"there is {_3d} 3d processing algorithms.")
print(f"there is {len(others)} others processing algorithms.")
print(f"The total is {qgis + native + saga + grass + gdal + _3d + len(others)} processing algorithms.")

for alg in QgsApplication.processingRegistry().algorithms():
    m = re.search(r'(^;*)\w+', alg.id()).group(0)
    if m == "qgis":
        print(alg.id())