1. About QGIS
.. 1. What is a GIS
.. 2. What is QGIS
..... 1. QGIS interface
2. A word on geospatial Data
.. 1. Vector data
.. 2. Raster data
.. 3. specific formats (LAS, GPX)
.. 4. Datasets
3. Vector data handling
.. 1. Load vector data
.. 2. Use the attribute table
.. 3. Join datasets
..... 1. Join by attributes
..... 2. Spatial join
.. 4. Symbology
..... 1. graphic semiology
.. 5. Data processing
4. Raster data handling
.. 1. Load raster data
.. 2. Image composition
.. 3. Raster calculator
5. Create a map
.. 1. Cartography basics
.. 2. Add a map
.. 3. Add a Legend
.. 4. Add a title
.. 5. Add a Scale
.. 6. Add an orientation arrow
.. 7. Add metadata (author, date, sources, licence, etc…)
6. Going further
.. 1. Plugins
.. 2. Graphical modeler
.. 3. Atlas generation
.. 4. PyQGIS
7. Ressources
.. 1. In english
.. 2. In French
