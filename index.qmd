# Preface {.unnumbered}

[![](images/qgis-logo.svg){fig-alt="QGIS logo" fig-align="center"}](https://qgis.org/en/site/)

This is document is an introduction to [QGIS](https://qgis.org/en/site/) for [Gustave Eiffel university](https://www.univ-gustave-eiffel.fr) PhD candidates and personnels.

It can be used by anyone.

 
Rendered pages available here: [ame.gitpages.univ-eiffel.fr/getting-started-with-qgis](https://ame.gitpages.univ-eiffel.fr/getting-started-with-qgis)

This handout is based on the French QGIS tutorial created by [Julie Pierson - LETG/CNRS](https://tutoqgis.cnrs.fr/).
We will use the same data as the one provided by the tutorial: https://tutoqgis.cnrs.fr/donnees/TutoQGIS_Donnees.zip


Screenshots are original content from the author, some illustrations are from Wikimedia (author names are in captions, click on image links to source url). All QGIS icons are from QGIS official documentation.

[![](images/logo_univ_gustave_eiffel_rvb.svg){fig-alt="Univ. Eiffel logo" fig-align="center"}](https://www.univ-gustave-eiffel.fr/)


# Licence 
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

# Contributors

- Nicolas Roelandt - Univ. Eiffel/AME
