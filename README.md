# Introduction to QGIS 

This document is one day training session handout for Gustave Eiffel university PhD candidates and personnels 

It can be used by anyone.

 
Rendered pages available here: https://ame.gitpages.univ-eiffel.fr/getting-started-with-qgis/

This handout is based on the French QGIS tutorial created by (Julie Pierson - LETG/CNRS)[https://tutoqgis.cnrs.fr/].
We will use the same data as the one provided by the tutorial: https://tutoqgis.cnrs.fr/donnees/TutoQGIS_Donnees.zip

# Licence 
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

# Contributors

- Nicolas Roelandt - Univ. Eiffel/AME
