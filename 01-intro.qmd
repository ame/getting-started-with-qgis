---
title: "About QGIS"
---

## What is a GIS ?

A GIS is a Geographical Information System, it is a tool or a collection of tools that can be used to collect, store, analyse, visualize and manage geospatial data.

## What is QGIS ?

QGIS is a full-featured desktop GIS that you can install on your computer and with which you can perform the most common operations with geospatial data and produce maps.

It is a free and open-source software that your are free to use for any usage on any computer. You can even redistribute it.

It has been created in 2002 and since then has continued to improve until it is competitive with the tools of industrial editors.

## Interface

![QGIS interface](images/01_interface.png){fig-alt="QGIS interface screenshot with bordered areas" fig-align="center"}

1.  Project management (create new one, open, save, layout management)
2.  Zoom and pan buttons
3.  Add or create new layers
4.  Edit vector layers
5.  Selection tools
6.  Data source browser
7.  Layers management
8.  Recently opened projects
9.  News about the project
10. Project templates
11. Project information (cursor position, zoom, CRS, etc.).

There is also a toolbar at the top of the window with several entries:

-   `Project`: Manage projects (new, open, save, properties)

-   `Edit` : Edit layer's features (vector data only)

-   `View` : Zooms, interface layout

-   `Layer` : Add/copy/remove layers

-   `Settings` : User's profile, options

-   `Plugins` : Plugin manager and plugins access

-   `Vector` : Vector data processing tools

-   `Raster` : Raster data processing tools

-   `Database` : databases connections

-   `Web` : Metasearch plugin (search services metadata, not activated by default)

-   `Mesh` : Mesh calculator (3D point cloud)

-   `Processing` : Access to Processing toolbox, graphical modeler

-   `Help` : Help menu

If you click on the new project icon ![New project icon](images/mActionFileNew.png){fig-alt="New project icon in the shape of an A4 paper shet with the right corner folded"}, or use the menu `Project -> New`, the interface will change slightly to replace the Recent projects and news blocks to display the map view. Other elements remains present.

![Map view](images/02_canvas.png){fig-alt="QGIS main interface with the map/canvas view circled in red" fig-align="center"}

1.  Map view (also called *canvas*)

The large area is called the *Map View* or the *Map Canvas*, this where your layer will be displayed.

The layers aspect might vary regarding the rendering you applied to it (symbology, labelling, etc.) and the *Coordinates Reference System* (See @sec-crs for more information about it) defined.

You can find a detailed description of the interface and all the buttons in the official [documentation](https://docs.qgis.org/latest/en/docs/user_manual/introduction/qgis_gui.html).

## QGIS projects

QGIS works in project mode. A project will save the loaded layers and their symbology. It will also save layouts created with the layout manager.

::: callout-caution
The project only keeps paths to the files.

Layers created with a geoprocessing tool and saved in memory ![New project icon](images/mIndicatorMemory.png){fig-alt="Layer in memory icon in shape of a "} will be lost when QGIS will be closed. But still be listed.
:::
